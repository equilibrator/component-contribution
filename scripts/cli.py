"""Command line script for managing groups in equilibrator-cache."""
# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import logging

import click
import click_log
import pandas as pd
from equilibrator_cache import DEFAULT_COMPOUND_CACHE_SETTINGS, Compound
from equilibrator_cache.api import create_compound_cache_from_sqlite_file
from equilibrator_cache.thermodynamic_constants import default_pMg, default_T
from equilibrator_cache.zenodo import get_cached_filepath
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm

from component_contribution import DEFAULT_CC_PARAMS_SETTINGS
from component_contribution.trainer import Trainer
from component_contribution.training_data import FullTrainingDataFactory


logger = logging.getLogger()
click_log.basic_config(logger)
Session = sessionmaker()


@click.group()
@click.help_option("--help", "-h")
@click_log.simple_verbosity_option(
    logger,
    default="INFO",
    show_default=True,
    type=click.Choice(["CRITICAL", "ERROR", "WARN", "INFO", "DEBUG"]),
)
def cli():
    """Command line interface to populate and update the equilibrator cache."""
    pass


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--ccache_local",
    metavar="LOCAL_CACHE",
    default=None,
    show_default=True,
    help="The path to a local version of the compound cache (.sqlite file). "
    "By default, uses the version stored on Zenodo.",
)
@click.option(
    "--batch-size",
    type=int,
    default=30000,
    show_default=True,
    help="The size of batches of compounds considered at a time.",
)
def decompose(
    ccache_local: str,
    batch_size: int,
) -> None:
    """Calculate and store thermodynamic information for compounds."""

    from equilibrator_assets.group_decompose import (
        GroupDecomposer,
        GroupDecompositionError,
    )
    from equilibrator_assets.molecule import Molecule, OpenBabelError

    if ccache_local is None:
        path = get_cached_filepath(DEFAULT_COMPOUND_CACHE_SETTINGS)
    else:
        path = ccache_local
    engine = create_engine(f"sqlite:///{path}")
    session = Session(bind=engine)

    # Query for all compounds.
    query = session.query(
        Compound.id, Compound.inchi_key, Compound.smiles
    ).filter(Compound.smiles.isnot(None), Compound.group_vector.is_(None))

    logger.debug("decomposing all compounds with structures")
    input_df = pd.read_sql_query(query.statement, query.session.bind)

    group_decomposer = GroupDecomposer()

    with tqdm(total=len(input_df), desc="Analyzed") as pbar:
        for index in range(0, len(input_df), batch_size):
            view = input_df.iloc[index : index + batch_size, :]
            compounds = []
            for row in view.itertuples(index=False):
                try:
                    # it is important to use here the SMILES representation
                    # of the molecule, and not the InChI, since it hold the
                    # information about the most abundant species at pH 7.
                    mol = Molecule.FromSmiles(row.smiles)
                    decomposition = group_decomposer.Decompose(
                        mol, ignore_protonations=False, raise_exception=True
                    )
                    group_vector = decomposition.AsVector()
                    compounds.append(
                        {"id": row.id, "group_vector": list(group_vector.flat)}
                    )
                    logger.debug(
                        "Decomposition of Compound(id=%d, inchi_key=%s): %r",
                        row.inchi_key,
                        row.id,
                        decomposition,
                    )
                except OpenBabelError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))
                except GroupDecompositionError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))

            session.bulk_update_mappings(Compound, compounds)
            session.commit()
            pbar.update(len(view))

    print(
        f"The DB file ({path}) has been updated. Please submit it as a new "
        "version to Zenodo."
    )


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--ccache_local",
    metavar="LOCAL_CACHE",
    default=None,
    show_default=True,
    help="The path to a local version of the compound cache (.sqlite file). "
    "By default, uses the version stored on Zenodo.",
)
@click.option(
    "--path_to_tecrdb",
    metavar="TECRDB_PATH",
    default=None,
    show_default=True,
    help="The path to a local version of the TECR database. "
    "By default, uses the version stored on Zenodo.",
)
@click.option(
    "--path_to_redox",
    metavar="REDOX_PATH",
    default=None,
    show_default=True,
    help="The path to a local version of the reduction potential database. "
    "By default, uses the version stored on Zenodo.",
)
@click.option(
    "--path_to_dgf",
    metavar="DGF_PATH",
    default=None,
    show_default=True,
    help="The path to a local version of the formation energy database. "
    "By default, uses the version stored on Zenodo.",
)
@click.option(
    "--ignore_magnesium",
    is_flag=True,
    default=False,
    show_default=True,
    help="Use this flag if you want to ignore [Mg2+] ions during training.",
)
@click.option(
    "--ignore_temperature",
    is_flag=True,
    default=False,
    show_default=True,
    help="Use this flag if you want to ignore temperature during training.",
)
def train(
    ccache_local: str,
    path_to_tecrdb: str,
    path_to_redox: str,
    path_to_dgf: str,
    ignore_magnesium: bool,
    ignore_temperature: bool,
) -> None:
    """Train the Component Contribution model and push to quilt."""
    if ccache_local is None:
        path = get_cached_filepath(DEFAULT_COMPOUND_CACHE_SETTINGS)
    else:
        path = ccache_local
    ccache = create_compound_cache_from_sqlite_file(path)

    train_data = FullTrainingDataFactory(ccache=ccache).make(
        path_to_tecrdb,
        path_to_redox,
        path_to_dgf,
        override_p_mg=(default_pMg if ignore_magnesium else None),
        override_temperature=(default_T if ignore_temperature else None),
    )
    params = Trainer.train(train_data)
    params.to_npz(DEFAULT_CC_PARAMS_SETTINGS.filename)


if __name__ == "__main__":
    cli()
